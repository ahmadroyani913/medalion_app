
const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/index.vue') }
    ]
  },
  {
    path: '/rules',
    component: () => import('../pages/investor_rules/InvestorRules.vue')
  },
  {
    path: '/mastertrader',
    component: () => import('../pages/master_trader/MasterTrader.vue')
  },
  {
    path: '/beforelogregis',
    component: () => import('../pages/log_regis/BeforeLoginRegister.vue')
  },
  {
    path: '/splash',
    component: () => import('../pages/splash/SplashPage.vue')
  },
  {
    path: '/coba',
    component: () => import('../pages/investor_rules/Coba.vue')
  },

  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes
