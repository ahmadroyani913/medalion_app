const BASE_URL = 'http://trade.patramas.co.id/mobile'
    // const BASE_URL_PATRA = 'https://api.xsmts.xyz:7778/mmaster/'
    // const BASE_URL_PATRA = 'https://api.xsmts.xyz/mmaster/'
    // const BASE_URL_PATRA = 'http://172.26.11.11/mmaster/'
const BASE_URL_PATRA = (process.env.DEV) ? 'http://172.26.11.11/mmaster/' : 'https://api.xsmts.xyz:7778/mmaster/'
    // const BASE_URL_PATRA = '/mmaster/'

const APPLICATION_TYPE_CODE = 'pub-mmaster'
const BUILD_VERSION = '2020/07/27 11:59:00'

// API AUTH
const AUTHENTICATE_TOKEN = '/api/sec/authenticateToken'
const AUTHENTICATE_URL = '/api/sec/authenticate'
const ACCOUNT_HISTORY_URL = '/api/accountHistories'
const CHANGE_ROLE_URL = '/api/sec/changeRole'
const SIGN_UP_URL = '/api/public/signUp'
const CHANGE_PASSWORD_URL = '/api/myuser/changePassword'
    // const UPLOAD_PROFILE_URL = BASE_URL + '/api/myuser/pictureProfile'
const UPLOAD_PROFILE_URL = '/api/myuser/pictureProfile'
const GET_PROFILE_URL = '/api/myuser/pictureProfile/'

// API SSE
const OPEN_ORDER_SSE_URL = BASE_URL_PATRA + 'api/master/tradeResult/openTrade'
const OPEN_ORDER_SSE_URL_FOLLOWER = BASE_URL_PATRA + 'api/follower/tradeResult/openTrade'

// BROKER
const SIGN_UP_BROKER_URL = '/api/register/account/register'
const GET_BROKER_ALL = '/api/broker/all'
const GET_SIMBOL = '/api/broker/'
const GET_SIMBOL_CODE = '/api/register/account/currencySymbol'

// API Assets
const CHART_DATASET = '/api/asset/tree/'
const POST_ASSET_DETAIL = '/api/asset/detail/'
const EDIT_ASSET_DETAIL = '/api/asset/detail/'
const GET_ASSET_SYMBOL = '/api/asset/detail/option/symbol/'

// API Trade result master
const TRADE_RESULT_DAY_MASTER = '/api/master/tradeResult/day'
const TRADE_RESULT_MONTH_MASTER = '/api/master/tradeResult/month'
const TRADE_RESULT_WEEK_MASTER = '/api/master/tradeResult/week'
const TRADE_RESULT_YEAR_MASTER = '/api/master/tradeResult/year'

// API Trade result Follower
const TRADE_RESULT_DAY_FOLLOWER = '/api/follower/tradeResult/day'
const TRADE_RESULT_MONTH_FOLLOWER = '/api/follower/tradeResult/month'
const TRADE_RESULT_WEEK_FOLLOWER = '/api/follower/tradeResult/week'
const TRADE_RESULT_YEAR_FOLLOWER = '/api/follower/tradeResult/year'


// API Device
const DEVICE_MANAGEMENT = 'api/device/getAll'

// API History
const HISTORY_FOLLOWERS = '/api/follower/tradeResult/detailHist'
const HISTORY_MASTER = '/api/master/tradeResult/detailHist'

// API Comment (Trade Journal)
const COMMENT_URL = '/api/comment/updateTrdAccountHistoryComment'
const COMMENT_LIST_URL = '/api/comment/commentList'

// API Trade Rules
const GET_ALL_RULES = '/api/appTradeRule/all'

// API Trade Plan
const GET_ALL_PLAN = '/api/appTradePlan/all'
const POST_PLAN = '/api/appTradePlan/create'
const PUT_PLAN = '/api/appTradePlan/update/'
const GET_BY_CODE_PLAN = '/api/appTradePlan/byMstCode/'

// API Account
const GET_ACCOUNT_MASTER = '/api/account/master'
const GET_ACCOUNT_FOLLOWER = '/api/account/follower'

// API Market Review
const MARKET_REVIEW = '/api/marketReview/create'
const GET_ALL_MARKET_REVIEW = '/api/marketReview/all'

// API My Master
const MY_MASTER = '/api/myMasterAccounts'


export { BUILD_VERSION, GET_SIMBOL_CODE, GET_SIMBOL, GET_BROKER_ALL, SIGN_UP_BROKER_URL, MY_MASTER, OPEN_ORDER_SSE_URL_FOLLOWER, TRADE_RESULT_DAY_FOLLOWER, TRADE_RESULT_WEEK_FOLLOWER, TRADE_RESULT_YEAR_FOLLOWER, TRADE_RESULT_MONTH_FOLLOWER, TRADE_RESULT_YEAR_MASTER, TRADE_RESULT_MONTH_MASTER, TRADE_RESULT_DAY_MASTER, TRADE_RESULT_WEEK_MASTER, GET_ACCOUNT_MASTER, GET_ACCOUNT_FOLLOWER, HISTORY_MASTER, HISTORY_FOLLOWERS, AUTHENTICATE_TOKEN, GET_BY_CODE_PLAN, PUT_PLAN, POST_PLAN, GET_ALL_MARKET_REVIEW, MARKET_REVIEW, GET_ALL_PLAN, GET_ALL_RULES, COMMENT_LIST_URL, COMMENT_URL, DEVICE_MANAGEMENT, BASE_URL_PATRA, EDIT_ASSET_DETAIL, OPEN_ORDER_SSE_URL, UPLOAD_PROFILE_URL, CHANGE_PASSWORD_URL, SIGN_UP_URL, BASE_URL, APPLICATION_TYPE_CODE, AUTHENTICATE_URL, ACCOUNT_HISTORY_URL, CHANGE_ROLE_URL, CHART_DATASET, POST_ASSET_DETAIL, GET_ASSET_SYMBOL, GET_PROFILE_URL }