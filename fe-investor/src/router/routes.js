const routes = [
  {
    path: '/',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '',component: () => import('../pages/splash/SplashPage.vue')},
      { path: 'home', component: () => import('pages/home/Index.vue') },
      { path: 'account', component: () => import('pages/result/Index.vue') },
      { path: 'acountdetail', component: () => import('pages/accountdetail/Index.vue') },
      { path: 'investorrules',component: () => import('../pages/investor_rules/InvestorRules.vue')},
      { path: 'tradingrules',component: () => import('../pages/trading_rules/TradingRules.vue')},
      { path: 'about',component: () => import('../pages/about/Index.vue')},

    ]
  },
  {
    path: '/beforelogregis',
    component: () => import('../pages/log_regis/BeforeLoginRegister.vue')
  },
  {
    path: '/splash',
    component: () => import('../pages/splash/SplashPage.vue')
  },
  {
    path: '/login',
    component: () => import('layouts/screen.vue'),
    children: [
      { path: '', component: () => import('pages/login/login.vue') }
    ]
  },
  {
    path: '/register',
    component: () => import('layouts/screen.vue'),
    children: [
      { path: '', component: () => import('pages/login/register.vue') }
    ]
  },
  {
    path: '/profile',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/userProfil/userProfil.vue') }
    ]
  },
  {
    path: '/history',
    component: () => import('layouts/MainLayout.vue'),
    children: [
      { path: '', component: () => import('pages/history/history.vue') }
    ]
  },



  // Always leave this as last one,
  // but you can also remove it
  {
    path: '*',
    component: () => import('pages/Error404.vue')
  }
]

export default routes