import axios from 'axios'
import { BASE_URL } from './constant'
import { BASE_URL_PATRA } from './constant'

export function getApiResource(token){
    return axios.create({
        baseURL: BASE_URL,
        timeout: 10000,
        withCredentials: false,
        headers: {
            'Access-Control-Allow-Origin': 'http://172.26.10.56',
            'Content-Type': 'multipart/form-data',
            'Authorization': 'Bearer ' + token
        }
    })
}

export function getApiNoAuth(){
    return axios.create({
        baseURL: BASE_URL_PATRA,
        timeout: 10000,
        withCredentials: false,
        headers: {
            'Access-Control-Allow-Origin': 'http://172.26.10.56',
            'Content-Type': 'application/json'
        }
    })
}

export function getApiAuth(token){
    return axios.create({
        baseURL: BASE_URL,
        timeout: 10000,
        withCredentials: false,
        headers: {
            'Access-Control-Allow-Origin': 'http://172.26.10.56',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
    })
}

export function getApiAuthPatra(token){
    return axios.create({
        baseURL: BASE_URL_PATRA,
        timeout: 10000,
        withCredentials: false,
        headers: {
            'Access-Control-Allow-Origin': 'http://172.26.10.56',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + token
        }
    })
}


