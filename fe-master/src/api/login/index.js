import { getApiNoAuth } from '../utils'
import { AUTHENTICATE_URL } from '../constant'

export default {

    loginUser(window, loginParam) {
        return getApiNoAuth()
            .post(AUTHENTICATE_URL, loginParam)
            .then(function(response) {
                return response.data
            })
            .catch(function(err) {
                if (err.response) {
                    return err.response.status
                }
            })
    }
}