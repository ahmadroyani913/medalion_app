import Vue from 'vue'
import axios from 'axios'

// const BASE_URL = 'http://trade.patramas.co.id/mobile'
const BASE_URL_PATRA = 'http://172.26.11.11/mmaster/'
// const BASE_URL_PATRA = 'https://api.xsmts.xyz:7778/mmaster/'


// const BASE_URL_PATRA_MASTER = 'http://172.26.11.11/mmaster/'
const API_TIMEOUT = 10000

const base = axios.create({
    baseURL: BASE_URL_PATRA,
    timeout: API_TIMEOUT,
    headers: {
        'Access-Control-Allow-Origin': 'http://172.26.10.56',
        'Content-Type': 'application/json'
    }
});

Vue.prototype.$http = base

